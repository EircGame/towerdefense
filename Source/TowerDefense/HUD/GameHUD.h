// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "../Objects/AlliesSpawner.h"
#include "../Objects/MinionsSpawner.h"
#include "../Allies/QuarterTower.h"
#include "GameHUD.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AGameHUD : public AHUD
{
	GENERATED_BODY()

public:
	AGameHUD();

	void SetAlliesSpawner(AAlliesSpawner* spawner);
	void SetQuarterTower(AQuarterTower* tower);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawners")
	AAlliesSpawner* selectedAlliesSpawner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawners")
	AMinionsSpawner* minionsSpawner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Objects")
	AQuarterTower* quarterTower;
	
};
