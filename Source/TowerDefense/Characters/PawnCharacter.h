// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Characters/WalkCharacter.h"
#include "PawnCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API APawnCharacter : public AWalkCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APawnCharacter();
	
	
};
