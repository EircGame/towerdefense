// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "DefensorCharacter.h"

ADefensorCharacter::ADefensorCharacter()
{

	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	//Propriedades do personagem
	maxLife = 600.0;
	life = 600.0;
	baseSpeed = 250;
	workSpeedDrop = 100;
	isDefending = false;
}

void ADefensorCharacter::NotifyIsUnderAttack(bool isUnderAttack)
{
	lifeChanging = isUnderAttack;
	//No caso de defensor estar sendo atacado implica em ele defender
	if (isUnderAttack && !isDefending)
	{
		baseSpeed = baseSpeed - workSpeedDrop;
		isDefending = true;
	}
	else if (!isUnderAttack && isDefending)
	{
		baseSpeed = baseSpeed + workSpeedDrop;
		isDefending = false;
	}
}

void ADefensorCharacter::SetHeroStatus()
{
	Super::SetHeroStatus();

	//Editar status do minion aqui
	maxLife = 1000;
	life = 1000;
	baseSpeed = 300;
	workSpeedDrop = 50;
}