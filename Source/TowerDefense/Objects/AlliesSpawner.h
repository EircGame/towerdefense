// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "AlliesSpawner.generated.h"

UENUM(BlueprintType)
enum class EAlliesType : uint8
{
	A_HEALER		UMETA(DisplayName = "A_Healer"),
	A_DAMAGER		UMETA(DisplayName = "A_Damager"),
	A_QUARTER		UMETA(DisplayName = "A_Quarter")
};

UCLASS()
class TOWERDEFENSE_API AAlliesSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAlliesSpawner();

	//M�todo chamado para instanciar um objecto da classe passada
	UFUNCTION(BlueprintCallable, Category = "Spawn")
		void SpawnAlly(EAlliesType type);


private:
	UClass* BP_HealingTower;
	UClass* BP_DamagerTower;
	UClass* BP_QuarterTower;
	
};
