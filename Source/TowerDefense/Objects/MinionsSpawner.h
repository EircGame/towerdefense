// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../MapComponents/PathPoint.h"
#include "../Characters/DefensorCharacter.h"
#include "../Characters/RangeCharacter.h"
#include "../Characters/PawnCharacter.h"
#include "../Characters/SpyCharacter.h"
#include "../Characters/WalkCharacter.h"
#include "../GameState/MyGameState.h"
#include "GameFramework/Actor.h"
#include "MinionsSpawner.generated.h"

UCLASS()
class TOWERDEFENSE_API AMinionsSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMinionsSpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Spline component que o minions instanciado ir� seguir
	UPROPERTY(EditAnywhere, Category = "Must Set")
	APathPoint* spline;
	
	//M�todo chamado para instanciar um objecto da classe passada
	UFUNCTION(BlueprintCallable, Category = "Spawn")
	void SpawnMinion(ECharactersType type, bool hasHeroStatus);

private:
	//Blueprints dos tipos de minions
	UClass* BP_defensor;
	UClass* BP_range;
	UClass* BP_spy;
	UClass* BP_pawn;

	UBoxComponent* box;
};
