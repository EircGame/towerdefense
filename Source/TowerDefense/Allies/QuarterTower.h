// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Allies/StaticAlly.h"
#include "../Characters/DefensorCharacter.h"
#include "../Characters/PawnCharacter.h"
#include "../Characters/RangeCharacter.h"
#include "../Characters/SpyCharacter.h"
#include "../Characters/WalkCharacter.h"
#include "../Objects/MinionsSpawner.h"
#include "QuarterTower.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FTowerStateChanged, bool, newQuarterState, AQuarterTower*, quarter);

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AQuarterTower : public AStaticAlly
{
	GENERATED_BODY()
	
public:
	AQuarterTower();

	//Callback para informar que o status da torre mudou
	FTowerStateChanged OnStatusChanged;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	//Pega todos os AWalkingCharacter dentro do range e coloca na sua respectiva lista
	UFUNCTION(BlueprintCallable, Category = "Events")
		void OpenQuarter();

	//Para de colocar characters dentro do quartel
	UFUNCTION(BlueprintCallable, Category = "Events")
		void CloseQuarter();

	UFUNCTION(BlueprintCallable, Category = "Events")
		void ReleaseMinion(ECharactersType type);

	//Atualiza a lista de targets. Deve ser chamado no blueprint pelo evento
	//ActorBeginOverlap
	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnEnterAreaOfAction(AWalkCharacter* target);

	//Atualiza a lista de targets. Deve ser chamado no blueprint pelo evento
	//ActorEndOverlap
	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnExitAreaOfAction(AWalkCharacter* target);

	UFUNCTION(BlueprintCallable, Category = "State")
		bool GetQuarterState() { return isQuarterOpen; };

	UFUNCTION(BlueprintCallable, Category = "Count")
		int32 GetTotalOf(ECharactersType type);

	UFUNCTION()
		void PutInQuarter(AWalkCharacter* target);

	void SetCharacterToQuarter(AWalkCharacter* character);

protected:
	bool isQuarterOpen;
	TArray<ASpyCharacter*> spiesList;
	TArray<ARangeCharacter*> rangesList;
	TArray<ADefensorCharacter*> defensorsList;
	TArray<APawnCharacter*> pawnsList;

private:
	void SetCharacterBackToPath(AWalkCharacter* character);
	//Metodo chamada para destruir qualquer minion dentro da torre pois essa foi destruida
	void DestroyMinionsInside();
	
};
