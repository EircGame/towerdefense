// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Allies/StaticAlly.h"
#include "../Enemies/StaticEnemy.h" 
#include "../Characters/Objects/SimpleProjectile.h" 
#include "AttackTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AAttackTower : public AStaticAlly
{
	GENERATED_BODY()

public:
	AAttackTower();

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "Events")
		void OnEnterAreaOfAttack(AStaticEnemy * target);

	UFUNCTION(BlueprintCallable, Category = "Events")
		void OnExitAreaOfAttack(AStaticEnemy * target);

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void SetTowerMeshExtent(FVector BoxExtent);

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void SetTowerMesh(UStaticMeshComponent* mesh);

	FVector const GetTowerMeshExtent();

protected:
	TArray<AStaticEnemy*> listOfEnemies;
	//Definido em tiros/segundos
	float attackRate;
	AStaticEnemy* currTarget;
	//Armazena o tempo passado entre ticks
	float accumulatedTime;
	//Dano
	int damage;

	//Projetil
	UClass* BP_SimpleProjectile;

	//Informacoes da mesh
	FVector towerMeshBoxExtent;
	UStaticMeshComponent* towerMesh;

	AStaticEnemy* SelectTarget();
	void Attack();
};
