// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "../Characters/WalkCharacter.h"
#include "HealingTower.h"

AHealingTower::AHealingTower()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	actionRate = 2;
	accumulatedTime = 0;
	healAmount = 50;
	life = 300;
	maxLife = life;
}

void AHealingTower::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//Se j� passou o tempo attackRate desde o �ltimo attack e tem algum algo ent�o ataca
	if (accumulatedTime > 1 / actionRate)
	{
		accumulatedTime = 0;
		Heal();
	}
	//Se j� tem um alvo ent�o incrementa o tempo acumulado
	else
	{
		accumulatedTime += DeltaSeconds;
	}
}

//Cura todos os walkcharacters que est�o na sua �rea
void AHealingTower::Heal()
{
	for (AWalkCharacter* actor : listOfCharacters)
	{
		if (actor)
		{
			//Se o actor atingiu o maximo de vida seta o maximo
			if (actor->life + healAmount > actor->maxLife)
			{
				actor->life = actor->maxLife;
			}
			//Se n�o s� cura
			else
			{
				actor->life = actor->life + healAmount;
			}
		}
	}
}