// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SpectatorPawn.h"
#include "CameraPawn.generated.h"

UCLASS()
class TOWERDEFENSE_API ACameraPawn : public ASpectatorPawn
{
	GENERATED_BODY()

	//Camera Component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* topDownCameraComponent;

	//"Bra�o" que segura a camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* cameraBoom;

public:
	// Sets default values for this pawn's properties
	ACameraPawn();

	void ChangeCameraArmLength(float changeValue);
	void RotateCameraArm(FRotator rotation);
	void MoveCharacterForward(float changeValue);
	void MoveCharacterRight(float changeValue);
	void MoveToLoc(FVector location);
	
};
