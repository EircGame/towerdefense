// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "GameStateDao.h"


UGameStateDao::UGameStateDao()
{
	savePath = FPaths::GameDir() + "\\mysavefile.save";
}

bool UGameStateDao::loadState(UGameStateVo & gameStateVo)
{
	TArray<uint8> theBinaryArray;
	if (!FFileHelper::LoadFileToArray(theBinaryArray, *savePath))
	{
		//ClientMessage("FFILEHELPER:>> Invalid File");
		return false;
		//~~
	}

	//File Load Error
	if (theBinaryArray.Num() <= 0) return false;

	//~
	//		  Read the Data Retrieved by GFileManager
	//~

	FMemoryReader fromBinary = FMemoryReader(theBinaryArray, true); //true, free data after done
	fromBinary.Seek(0);
	loadData(fromBinary, gameStateVo);

	//~
	//Clean up 
	//~
	fromBinary.FlushCache();

	// Empty & Close Buffer 
	theBinaryArray.Empty();
	fromBinary.Close();

	return true;
}

bool UGameStateDao::saveState(UGameStateVo& gameStateVo)
{
	FBufferArchive toBinary;
	saveData(toBinary, gameStateVo);

	if (FFileHelper::SaveArrayToFile(toBinary, *savePath))
	{
		// Free Binary Array 	
		toBinary.FlushCache();
		toBinary.Empty();

		//ClientMessage("Save Success!");
		return true;
	}

	// Free Binary Array 	
	toBinary.FlushCache();
	toBinary.Empty();

	return false;
}

void UGameStateDao::saveData(FArchive & ar, UGameStateVo& gameStateVo)
{

	ar << gameStateVo.totalOfSpies;
	ar << gameStateVo.totalOfDefensors;
	ar << gameStateVo.totalOfRanges;
	ar << gameStateVo.totalOfPawns;
	ar << gameStateVo.totalOfMinionsAvaiableOnMatch;
	ar << gameStateVo.difficultLevel;
	ar << gameStateVo.currLevel;
	ar << gameStateVo.currState;
	ar << gameStateVo.currChapter;
	ar << gameStateVo.heroType;
	ar << gameStateVo.canUseHero;
	ar << gameStateVo.totalOfMinionsOnBegin;
	ar << gameStateVo.matchRating;

	//Backup sendo salvo
	ar << gameStateVo.backupHeroType;
	ar << gameStateVo.backupCanUseHero;
	ar << gameStateVo.backupCurrChapter;
	ar << gameStateVo.backupCurrLevel;
	ar << gameStateVo.backupTotalOfDefensors;
	ar << gameStateVo.backupTotalOfPawns;
	ar << gameStateVo.backupTotalOfRanges;
	ar << gameStateVo.backupTotalOfSpies;
	ar << gameStateVo.backupTotalOfMinionsOnBegin;
}

void UGameStateDao::loadData(FArchive & ar, UGameStateVo& gameStateVo)
{
	ar << gameStateVo.totalOfSpies;
	ar << gameStateVo.totalOfDefensors;
	ar << gameStateVo.totalOfRanges;
	ar << gameStateVo.totalOfPawns;
	ar << gameStateVo.totalOfMinionsAvaiableOnMatch;
	ar << gameStateVo.difficultLevel;
	ar << gameStateVo.currLevel;
	ar << gameStateVo.currState;
	ar << gameStateVo.currChapter;
	ar << gameStateVo.heroType;
	ar << gameStateVo.canUseHero;
	ar << gameStateVo.totalOfMinionsOnBegin;
	ar << gameStateVo.matchRating;

	//Recupera backup
	ar << gameStateVo.backupHeroType;
	ar << gameStateVo.backupCanUseHero;
	ar << gameStateVo.backupCurrChapter;
	ar << gameStateVo.backupCurrLevel;
	ar << gameStateVo.backupTotalOfDefensors;
	ar << gameStateVo.backupTotalOfPawns;
	ar << gameStateVo.backupTotalOfRanges;
	ar << gameStateVo.backupTotalOfSpies;
	ar << gameStateVo.backupTotalOfMinionsOnBegin;

}



