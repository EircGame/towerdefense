// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "MyGameMode.h"
#include "MyGameState.h"
#include "MyPlayerController.h"

AMyGameMode::AMyGameMode()
{
	//Setamos qual player controller ser� usado
	PlayerControllerClass = AMyPlayerController::StaticClass();
	GameStateClass = AMyGameState::StaticClass();
	//Carregar o BP do pawn
	static ConstructorHelpers::FObjectFinder<UClass>
		CameraObj(TEXT("Class'/Game/TopDownCPP/Blueprints/GameState/BP_CameraPawn.BP_CameraPawn_C'"));
	if (CameraObj.Object)
	{
		DefaultPawnClass = CameraObj.Object;
	}
}
