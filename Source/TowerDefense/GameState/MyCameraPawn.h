// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SpectatorPawn.h"
#include "MyCameraPawn.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMyCameraPawn : public ASpectatorPawn
{
	GENERATED_BODY()
	
public:
	AMyCameraPawn();

	//Camera Component
	class UCameraComponent* topDownCameraComponent;

	//"Bra�o" que segura a camera
	class USpringArmComponent* cameraBoom;

	void ChangeCameraArmLength(float changeValue);
	void RotateCameraArm(FRotator rotation);
	void MoveCharacterForward(float changeValue);
	void MoveCharacterRight(float changeValue);

	
	
};
