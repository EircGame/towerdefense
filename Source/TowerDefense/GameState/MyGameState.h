// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "Utils/GameStateDao.h"
#include "Utils/GameStateVo.h"
#include "MyGameState.generated.h"

UENUM(BlueprintType)
enum class EGameStates : uint8
{
	GS_WIN			UMETA(DisplayName = "GS_Win"),
	GS_LOSE			UMETA(DisplayName = "GS_Lose"),
	GS_LOADGAME		UMETA(DisplayName = "GS_LoadGame"),
	GS_NEWGAME		UMETA(DisplayName = "GS_NewGame"),
	GS_LOADSTATE	UMETA(DisplayName = "GS_LoadState"),
	GS_LOADLEVEL	UMETA(DisplayName = "GS_LoadLevel"),
	GS_ONMENU		UMETA(DisplayName = "GS_OnMenu"),
	GS_PLAYING		UMETA(DisplayName = "GS_Playing"),
	GS_ERROR		UMETA(DisplayName = "GS_ERROR")
};

UENUM(BlueprintType)
enum class ECharactersType : uint8
{
	T_RANGE			UMETA(DisplayName = "T_Range"),
	T_DEFENSOR		UMETA(DisplayName = "T_Defensor"),
	T_SPY			UMETA(DisplayName = "T_Spy"),
	T_PAWN			UMETA(DisplayName = "T_Pawn"),
	T_HERO			UMETA(DisplayName = "T_Hero"),
	T_ERROR			UMETA(DisplayName = "T_ERROR")
};


UCLASS()
class TOWERDEFENSE_API AMyGameState : public AGameState
{
	GENERATED_BODY()
	
	
public:
	AMyGameState();

	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool updateSpiesCount();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool updateDefensorsCount();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool updatePawnsCount();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool updateRangesCount();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool updateCanUseHero();
	
	void updateCountMinionsEnd(ECharactersType type);

	UFUNCTION(BlueprintCallable, Category = "GameState")
		int32 getSpiesCount() { return gameStateVo->totalOfSpies; };
	UFUNCTION(BlueprintCallable, Category = "GameState")
		int32 getDefensorsCount() { return gameStateVo->totalOfDefensors; };
	UFUNCTION(BlueprintCallable, Category = "GameState")
		int32 getPawnsCount() { return gameStateVo->totalOfPawns; };
	UFUNCTION(BlueprintCallable, Category = "GameState")
		int32 getRangesCount() { return gameStateVo->totalOfRanges; };
	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool getCanUseHero() { return gameStateVo->canUseHero; };
	UFUNCTION(BlueprintCallable, Category = "GameState")
		ECharactersType GetHeroType() { return getCharacterTypeAsEnum(gameStateVo->heroType); };

	UFUNCTION(BlueprintCallable, Category = "GameState")
		int32 getCurrChapter() { return gameStateVo->currChapter; };

	UFUNCTION(BlueprintCallable, Category = "GameStatus")
		float getMatchRating() { return gameStateVo->matchRating; };

	UFUNCTION(BlueprintCallable, Category = "GameState")
		void convertTo(ECharactersType type);

	UFUNCTION(BlueprintCallable, Category = "GameState")
		EGameStates getCurrState() { return getGameStateAsEnum(gameStateVo->currState); };

	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool saveState();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		void loadGame();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		void newGame();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		void retryGame();

	UFUNCTION(BlueprintCallable, Category = "GameState")
		bool hasSavedGame();

	UFUNCTION(BlueprintCallable, Category = "GameDifficult")
		void setDifficultLevel(int32 difficult);

	UFUNCTION(BlueprintCallable, Category = "GameState")
		void setState(EGameStates newState);

	UFUNCTION(BlueprintCallable, Category = "GameQuitting")
		void quitting();

	void notifyMinionDeath();

private:
	UGameStateVo* gameStateVo;
	UGameStateDao* gameStateDao;

	void execState();
	int32 getGameStateAsInt(EGameStates state);
	EGameStates getGameStateAsEnum(int32 state);
	int32 getCharacterTypeAsInt(ECharactersType type);
	ECharactersType getCharacterTypeAsEnum(int32 type);

	//Carrega um level
	void openLevel(FName levelName);

	const int32 WIN_GS = 10;
	const int32 LOSE_GS = 11;
	const int32 LOADGAME_GS = 12;
	const int32 NEWGAME_GS = 13;
	const int32 LOADSTATE_GS = 14;
	const int32 LOADLEVEL_GS = 15;
	const int32 ONMENU_GS = 16;
	const int32 PLAYING_GS = 17;

	FString savePath = "E:\\mysavefile.save";
};
