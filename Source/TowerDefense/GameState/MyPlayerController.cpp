// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "MyPlayerController.h"
#include "../Enemies/StaticEnemy.h"
#include "../Objects/AlliesSpawner.h"
#include "CameraPawn.h"

AMyPlayerController::AMyPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Hand;
}

void AMyPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	if (bMouseBtnClicked)
	{
		//Atendemos um click por vez
		bMouseBtnClicked = false;
		CallAction();
	}
}

void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//Os nomes usados para os eventos s�o os mesmos que foram setados
	//la no project settings na aba de input
	//Eventos para o click
	InputComponent->BindAction("SetAction", IE_Pressed, this, &AMyPlayerController::OnSetActionPressed);

	//Eventos para touch
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMyPlayerController::OnSetActionTouched);

	InputComponent->BindAxis("MoveForward", this, &AMyPlayerController::OnMoveForwardAxis);
	InputComponent->BindAxis("MoveRight", this, &AMyPlayerController::OnMoveRightAxis);
	InputComponent->BindAction("ZoomIn", EInputEvent::IE_Pressed, this, &AMyPlayerController::OnZoomInAction);
	InputComponent->BindAction("ZoomOut", EInputEvent::IE_Pressed, this, &AMyPlayerController::OnZoomOutAction);
}

void AMyPlayerController::OnSetActionPressed()
{
	bMouseBtnClicked = true;
}

//Usando para touch
void AMyPlayerController::OnSetActionTouched(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult hitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, hitResult);
	if (hitResult.bBlockingHit)
	{
		AStaticEnemy* staticEnemy = Cast<AStaticEnemy>(hitResult.GetActor());
		AAlliesSpawner* alliesSpawner = Cast<AAlliesSpawner>(hitResult.GetActor());
		if (staticEnemy)
		{
			staticEnemy->NotifySelectedTower();
		}
		if (alliesSpawner)
		{
			// TODO: por hora vamos sempre criar uma torre de healing mas futuramente
			//deveremos informar a hud e abrir novos botoes na tela para ent�o depois tratar
			//o click e ver qual torre criar
			alliesSpawner->SpawnAlly(EAlliesType::A_HEALER);
		}
	}
}

void AMyPlayerController::CallAction()
{
	//Trace para ver o que est� sob o mouse
	FHitResult hitResult;
	GetHitResultUnderCursor(ECC_Visibility, false, hitResult);

	if (hitResult.bBlockingHit)
	{
		AStaticEnemy* staticEnemy = Cast<AStaticEnemy>(hitResult.GetActor());
		AAlliesSpawner* alliesSpawner = Cast<AAlliesSpawner>(hitResult.GetActor());
		AQuarterTower* quarterTower = Cast<AQuarterTower>(hitResult.GetActor());
		if (staticEnemy)
		{
			staticEnemy->NotifySelectedTower();
		}
		else if (alliesSpawner)
		{
			GetMyHUD()->SetAlliesSpawner(alliesSpawner);

			//Move a camera para a posi��o do ally
			APawn* const Pawn = GetPawn();
			ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
			if (cameraPawn)
			{
				cameraPawn->MoveToLoc(alliesSpawner->GetActorLocation());
			}
		}
		else if (quarterTower)
		{
			GetMyHUD()->SetQuarterTower(quarterTower);

			//Vemo a camera para a posi��o do ally
			APawn* const Pawn = GetPawn();
			ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
			if (cameraPawn)
			{
				cameraPawn->MoveToLoc(quarterTower->GetActorLocation());
			}
		}
	}
}

void AMyPlayerController::OnMoveForwardAxis(float axisValue)
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->MoveCharacterForward(axisValue);
	}
}

void AMyPlayerController::OnMoveRightAxis(float axisValue)
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->MoveCharacterRight(axisValue);
	}
}

void AMyPlayerController::OnZoomInAction()
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->ChangeCameraArmLength(-1.0f);
	}
}

void AMyPlayerController::OnZoomOutAction()
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->ChangeCameraArmLength(1.0f);
	}
}

AGameHUD * AMyPlayerController::GetMyHUD()
{
	//Na primeira vez que passa por aqui pode ser null entao recupera a HUD
	//Nao pode colocar no construtor pois a HUD nao ainda n�o ter sido instanciada
	if (myHUD == NULL)
	{
		myHUD = Cast<AGameHUD>(GetHUD());
	}
	return myHUD;
}
